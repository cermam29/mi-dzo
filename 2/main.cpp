#include <cmath>
#include <iostream>
#include <random>
#include <string>
#include <vector>    

#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "../stb_image.h"
#include "../stb_image_write.h"      

#include <fftw3.h>

using namespace std;

class Image {

   int width, height, channels;
   unsigned char* pixels;

public:

   Image(const string& file_name) {
      pixels = stbi_load(file_name.c_str(), &width, &height, &channels, 0);
   }

   ~Image() {
      stbi_image_free(pixels);
   }

   void fft() {
      fftw_complex* in = fftw_alloc_complex(width * height);
      fftw_complex* out = fftw_alloc_complex(width * height);
      auto plan = fftw_plan_dft_2d(height, width, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
      for (int c = 0; c < channels; ++c) {
         for (int i = c; i < width * height * channels; i += channels) {
            in[i / channels][0] = pixels[i] / 255.0;
            in[i / channels][1] = 0;
         }
         fftw_execute(plan);
         double max = 0;
         for (int i = 0; i < width * height; ++i) {
            out[i][0] = log(sqrt(out[i][0] * out[i][0] + out[i][1] * out[i][1]) + 1);
            if (out[i][0] > max) max = out[i][0];
         }
         for (int j = 0; j < height; ++j) {
            for (int i = 0; i < width; ++i) {
               int x = i <= width / 2 ? width / 2 - i : width - i + width / 2;
               int y = j <= height / 2 ? height / 2 - j : height - j + height / 2;
               in[i + j * width][0] = out[x + y * width][0];
            }
         }
         max = 255.0 / max;
         for (int i = c; i < width * height * channels; i += channels) {
            pixels[i] = in[i / channels][0] * max;
         }
      }
      fftw_destroy_plan(plan);
      fftw_free(in);
      fftw_free(out);
   }

   void Save(const string& file_name) {
      stbi_write_png(file_name.c_str(), width, height, channels, pixels, width * channels);
   }

};

int main() {
   Image image("lorem.png");
   image.fft();
   image.Save("lorem_fft.png");

   return 0;
}