#include <cmath>
#include <iostream>
#include <string>

#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "../stb_image.h"
#include "../stb_image_write.h"        

using namespace std;

struct Image {
   int width, height, channels;
   unsigned char* pixels;

   Image(const string& file_name) {
      pixels = stbi_load(file_name.c_str(), &width, &height, &channels, 0);
   }

   ~Image() {
      stbi_image_free(pixels);
   }

   void ChangeBrightness(double diff) {
      for (int i = 0; i < width * height * channels; ++i) {
         double tmp = pixels[i] / 255.0;
         tmp += diff;
         if (tmp > 1.0) tmp = 1.0;
         else if (tmp < 0.0) tmp = 0.0;
         pixels[i] = (unsigned char)(tmp * 255.0);
      }
   }

   void ChangeContrast(double diff) {
      for (int i = 0; i < width * height * channels; ++i) {
         double tmp = pixels[i] / 255.0;
         tmp *= diff;
         if (tmp > 1.0) tmp = 1.0;
         else if (tmp < 0.0) tmp = 0.0;
         pixels[i] = (unsigned char)(tmp * 255.0);
      }
   }

   void Equalize() {
      int sums[4][256];
      for (int c = 0; c < channels; ++c) {
         for (int i = 0; i < 256; ++i) sums[c][i] = 0;
      }
      for (int i = 0; i < width * height * channels;) {
         for (int c = 0; c < channels; ++c, ++i) {
            ++sums[c][pixels[i]];
         }
      }
      for (int c = 0; c < channels; ++c) {
         for (int i = 1; i < 256; ++i) {
            sums[c][i] += sums[c][i - 1];
            sums[c][i - 1] = (unsigned char)(255 * (int)sums[c][i - 1] / (width * height));
         }
      }
      for (int c = 0; c < channels; ++c) {
         sums[c][255] = (unsigned char)(255 * (int)sums[c][255] / (width * height));
      }
      for (int i = 0; i < width * height * channels;) {
         for (int c = 0; c < channels; ++c, ++i) {
            pixels[i] = sums[c][pixels[i]];
         }
      }
   }

   void Save(const string& file_name) {
      stbi_write_png(file_name.c_str(), width, height, channels, pixels, width * channels);
   }

};

int main() {
   cout << "Input file name: ";
   string input_file;
   cin >> input_file;
   Image image(input_file);

   int operation;
   cout << "1 = change brightness" << endl;
   cout << "2 = change contrast" << endl;
   cout << "3 = equalize" << endl;
   cout << "Operation: ";
   cin >> operation;
   double diff;
   switch (operation) {
   case 1:
      cout << "Brightness (-1.0, 1.0): ";
      cin >> diff;
      image.ChangeBrightness(diff);
      break;
   case 2:
      cout << "Contrast (0.0, +inf): ";
      cin >> diff;
      image.ChangeContrast(diff);
      break;
   case 3:
      image.Equalize();
      break;
   default:
      cerr << "Invalid operation" << endl;
      return 1;
   }

   cout << "Output file name (.png): ";
   string output_file;
   cin >> output_file;
   image.Save(output_file);

   return 0;
}