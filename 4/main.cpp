#include <cmath>
#include <iostream>
#include <random>
#include <string>
#include <vector>    

#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "../stb_image.h"
#include "../stb_image_write.h"        

using namespace std;

class Image {

   int width, height, channels;
   unsigned char* pixels, dummy;

   double Gauss(double x, double sigma_sqr) {
      return exp(-(x * x) / sigma_sqr);
   }

public:

   Image(const string& file_name) {
      pixels = stbi_load(file_name.c_str(), &width, &height, &channels, 0);
   }

   ~Image() {
      stbi_image_free(pixels);
   }

   void Bilateral(double s_sigma, double b_sigma) {
      // precalculate spacial gauss kernel
      int n = round(s_sigma);
      double s_sigma_sqr = s_sigma * s_sigma;
      double b_sigma_sqr = b_sigma * b_sigma;
      vector<double> gauss(2 * n + 1);
      for (int i = 0; i < 2 * n + 1; ++i) gauss[i] = Gauss(i - n, s_sigma_sqr);
      // create a zero-padded copy of the image
      int pwidth = width + 2 * n;
      int pheight = height + 2 * n;
      vector<unsigned char> padded(pwidth * pheight * channels, 0);
      for (int y = 0; y < height; ++y) {
         for (int i = 0; i < width * channels; ++i) {
            padded[(n + (y + n) * pwidth) * channels + i] = pixels[y * width * channels + i];
         }
      }
      // apply bilateral filter
      int idx = 0;
      for (int y = n; y < height + n; ++y) { // start from n since image is padded
         for (int x = n; x < width + n; ++x) {
            for (int c = 0; c < channels; ++c) {
               double weight_sum = 0, sum = 0;
               double value = padded[(x + y * pwidth) * channels + c]; // value of current pixel
               for (int dy = -n; dy <= n; ++dy) {
                  for (int dx = -n; dx <= n; ++dx) {
                     double s = gauss[dx + n] * gauss[dy + n]; // weight from spacial kernel
                     double d = padded[((x + dx) + (y + dy) * pwidth) * channels + c]; // value of neighbour pixel
                     double b = Gauss(value - d, b_sigma_sqr); // weight from similarity to current pixel
                     weight_sum += s * b;
                     sum += d * s * b;
                  }
               }
               pixels[idx++] = sum / weight_sum; // save computed pixel in the image
            }
         }
      }
   }       

   // used this to add noise to an image
   void AddGaussianNoise() {
      normal_distribution<double> dist(0, 8);
      mt19937_64 gen(42);
      for (int i = 0; i < width * height * channels; ++i) {
         int val = pixels[i] + dist(gen);
         if (val < 0) val = 0;
         else if (val > 255) val = 255;
         pixels[i] = val;
      }
   }

   void Save(const string& file_name) {
      stbi_write_png(file_name.c_str(), width, height, channels, pixels, width * channels);
   }

};

int main() {
   Image image("original.jpg");
   image.AddGaussianNoise();
   image.Save("noised.png");
   for (int s = 1; s < 10; s *= 3) {
      for (int b = 1; b < 101; b *= 10) {
         Image image("noised.png");
         image.Bilateral(s, b);
         image.Save(to_string(s) + "_" + to_string(b) + ".png");
      }
   }                                        
   return 0;
}