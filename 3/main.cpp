#include <cmath>
#include <iostream>
#include <string>
#include <vector>

#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "../stb_image.h"
#include "../stb_image_write.h"        

using namespace std;

const double S2PI = sqrt(2 * atan2(0, -1));

struct Image {
   int width, height, channels;
   unsigned char* pixels, dummy;

   Image(const string& file_name) {
      pixels = stbi_load(file_name.c_str(), &width, &height, &channels, 0);
   }

   ~Image() {
      stbi_image_free(pixels);
   }

   inline unsigned char& at(int x, int y, int c) {
      if (x < 0) return (dummy = at(0, y, c));
      else if (x >= width) return (dummy = at(width - 1, y, c));
      else if (y < 0) return (dummy = at(x, 0, c));
      else if (y >= height) return (dummy = at(x, height - 1, c));
      else return pixels[(x + y * width) * channels + c];
   }

   inline double dat(int x, int y, int c) {
      return at(x, y, c) / 255.0;
   }

   void GaussianKernel(double sigma) {
      int n = round(sigma) * 2 + 1, mi = n / 2;
      vector<double> gauss(n);
      double sum = 0;
      for (int i = 0; i < n; ++i) {
         gauss[i] = exp(-(i - mi)*(i - mi) / (2 * sigma * sigma)) / (S2PI * sigma);
         sum += gauss[i];
      }
      for (int i = 0; i < n; ++i) {
         gauss[i] /= sum;
      }
      for (int y = 0; y < height; ++y) {
         vector<unsigned char> v(width * channels);
         for (int x = 0; x < width; ++x) {
            vector<double> sum(channels, 0.0);
            for (int i = 0; i < n; ++i) {
               for (int c = 0; c < channels; c++) {
                  sum[c] += dat(x + i - mi, y, c) * gauss[i];
               }
            }
            for (int c = 0; c < channels; c++) {
               v[x * channels + c] = sum[c] * 255.0;
            }
         }
         for (int x = 0; x < width; ++x) {
            for (int c = 0; c < channels; c++) {
               at(x, y, c) = v[x * channels + c];
            }
         }
      }
      for (int x = 0; x < width; ++x) {
         vector<unsigned char> v(height * channels);
         for (int y = 0; y < height; ++y) {
            vector<double> sum(channels, 0.0);
            for (int i = 0; i < n; ++i) {
               for (int c = 0; c < channels; c++) {
                  sum[c] += dat(x, y + i - mi, c) * gauss[i];
               }
            }
            for (int c = 0; c < channels; c++) {
               v[y * channels + c] = sum[c] * 255.0;
            }
         }
         for (int y = 0; y < height; ++y) {
            for (int c = 0; c < channels; c++) {
               at(x, y, c) = v[y * channels + c];
            }
         }
      }       
   }       

   void Save(const string& file_name) {
      stbi_write_png(file_name.c_str(), width, height, channels, pixels, width * channels);
   }

};

int main() {
   Image image("original.jpg");
   image.GaussianKernel(100);
   image.Save("100.png");

   return 0;
}